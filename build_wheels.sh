#!/bin/bash
set -e -x

# Compile wheel
PYBIN="/opt/python/cp39-cp39/bin"
"${PYBIN}/pip" wheel . -v -w dist/ --no-deps
